const { request, response } = require('express')
const express = require('express')
const morgan = require('morgan')
const cors = require('cors')

const app = express()
app.use(express.json())
app.use(cors())

// logger
morgan.token('body', (req, res) => {
    if (req.method === 'POST') return `- ${JSON.stringify(req.body)}`
    return null
})
app.use(morgan(':method :url :status - :response-time ms :body'))

let persons = [
    {
    "name": "Arto Hellas",
    "number": "040-123456",
    "id": 1
    },
    {
    "name": "Ada Lovelace",
    "number": "39-44-5323523",
    "id": 2
    },
    {
    "name": "Dan Abramov",
    "number": "12-43-234345",
    "id": 3
    },
    {
    "name": "Mary Poppendieck",
    "number": "39-23-6423122",
    "id": 4
    }
]

app.get('/', (request, response) => {
    response.send('<h1>Hi</h1>')
})

app.get('/api/persons', (request, response) => {
    response.json(persons)
})

app.get('/info', (request, response) => {
    const context = `
    <div>
        <p>Phonebook has info for ${persons.length} people</p>
        <p>${new Date().toString()}</p>
    </div>
    `
    response.json(context)
})

app.get('/api/persons/:id', (request, response) => {
    const id = Number(request.params.id)
    const person = persons.find(person => person.id === id)

    if (person) {
        response.json(person)
    } else {
        response.status(404).end()
    }
})

app.delete('/api/persons/:id', (request, response) => {
    const id = Number(request.params.id)
    persons = persons.filter(person => person.id !== id)

    response.status(204).end()
})

const generateID = () => {
    const MAX = 10*10
    return Math.floor(Math.random() * MAX);
}

app.post('/api/persons', (request, response) => {
    const body = request.body

    // handle missing data
    if ( !body.name ) {
        return response.status(400).json({
            error: 'name missing'
        })
    } else if ( !body.number ) {
        return response.status(400).json({
            error: 'number missing'
        })
    }

    // repeated name
    if ( persons.find(person => person.name === body.name) ){
        return response.status(400).json({
            error: 'name must be unique'
        })
    }

    const person = {
        name: body.name,
        number: body.number,
        id: generateID()
    }

    persons = persons.concat(person)
    response.json(persons)
})

const unknownEndpoint = (request, response) => {
    response.status(404).send({ error: 'unknown endpoint' })
}

app.use(unknownEndpoint)

const PORT = 3001
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})